package ru.advantum.prototype.routebuilder.mvp;

import java.util.HashMap;
import java.util.Map;

public class PresenterCache {

    private Map<Class<?>, Presenter> mPresenters = new HashMap<>();
    private static PresenterCache sInstance;

    public void put(Class<?> key, Presenter presenter) {
        remove(key);
        mPresenters.put(key, presenter);
    }

    public <P extends Presenter> P get(Class<?> key) {
        return (P) mPresenters.get(key);
    }

    public void remove(Class<?> key) {
        Presenter presenter = mPresenters.get(key);
        if (presenter != null) {
            presenter.destroy();
        }
    }

    public static PresenterCache getInstance() {
        if (sInstance == null) {
            sInstance = new PresenterCache();
        }
        return sInstance;
    }

}
