package ru.advantum.prototype.routebuilder.logic;

import android.content.Context;

import ru.advantum.prototype.routebuilder.RouteApp;

/**
 * Created by yurchenko on 06.05.2016.
 */
public abstract class BaseLogic {

    protected static Context getContext() {
        return RouteApp.getInstance().getApplicationContext();
    }
}
