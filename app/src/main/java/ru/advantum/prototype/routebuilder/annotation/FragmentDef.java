package ru.advantum.prototype.routebuilder.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface FragmentDef {
    int resource() default 0;

    boolean busEnabled() default false;

    boolean knifeEnabled() default false;
}