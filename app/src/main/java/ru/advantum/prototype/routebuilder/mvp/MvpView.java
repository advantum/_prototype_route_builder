package ru.advantum.prototype.routebuilder.mvp;

import android.app.Activity;

public interface MvpView {
    Activity getActivity();

    void onError(Throwable throwable);
}
