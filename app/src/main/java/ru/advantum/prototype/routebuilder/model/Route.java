package ru.advantum.prototype.routebuilder.model;

import android.location.Location;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yurchenko on 06.05.2016.
 */
@DatabaseTable
public class Route {

    private static final long MOCK_ROUTE = -1;
    private static final long DEFAULT_STOP_TIME = 15000L;

    @DatabaseField(id = true, useGetSet = true)
    private long id = MOCK_ROUTE;

    @DatabaseField(useGetSet = true)
    private String title;

    @DatabaseField(useGetSet = true)
    private long defaultStopTime = DEFAULT_STOP_TIME;

    @DatabaseField(useGetSet = true, foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    private CheckPoint startPoint;

    @DatabaseField(useGetSet = true, foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    private CheckPoint endPoint;

    private List<CheckPoint> relocationList;

    private List<Location> registeredLocationList;

    public Route() {
        relocationList = new ArrayList<>();
        registeredLocationList = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getDefaultStopTime() {
        return defaultStopTime;
    }

    public void setDefaultStopTime(long defaultStopTime) {
        this.defaultStopTime = defaultStopTime;
    }

    public CheckPoint getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(CheckPoint startPoint) {
        this.startPoint = startPoint;
    }

    public CheckPoint getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(CheckPoint endPoint) {
        this.endPoint = endPoint;
    }

    public List<CheckPoint> getRelocationList() {
        return relocationList;
    }

    public void setRelocationList(List<CheckPoint> relocationList) {
        this.relocationList = relocationList;
    }

    public List<Location> getRegisteredLocationList() {
        return registeredLocationList;
    }

    public void setRegisteredLocationList(List<Location> registeredLocationList) {
        this.registeredLocationList = registeredLocationList;
    }

    public void addNextRelocation(CheckPoint item) {
        addRelocation(item, relocationList.size());
    }

    private void addRelocation(CheckPoint item, int position) {
        if (position == relocationList.size()) relocationList.add(item);
        else relocationList.add(position, item);
    }

    public void addLocation(Location location) {
        registeredLocationList.add(location);
    }
}
