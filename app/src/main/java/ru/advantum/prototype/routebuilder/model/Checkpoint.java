package ru.advantum.prototype.routebuilder.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by yurchenko on 06.05.2016.
 */
@DatabaseTable
public class CheckPoint {

    @DatabaseField(generatedId = true, useGetSet = true)
    private long id;
    @DatabaseField(useGetSet = true)
    private double lat;
    @DatabaseField(useGetSet = true)
    private double lng;
    @DatabaseField(useGetSet = true)
    private String title;
    @DatabaseField(useGetSet = true)
    private long stopTime;

    public CheckPoint() {
    }

    public CheckPoint(int id, String title, long stopTime, double lat, double lng) {
        this.id = id;
        this.title = title;
        this.stopTime = stopTime;
        this.lat = lat;
        this.lng = lng;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getStopTime() {
        return stopTime;
    }

    public void setStopTime(long stopTime) {
        this.stopTime = stopTime;
    }
}
