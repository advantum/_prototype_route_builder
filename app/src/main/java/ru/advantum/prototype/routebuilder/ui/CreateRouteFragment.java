package ru.advantum.prototype.routebuilder.ui;

import android.content.Context;

import butterknife.OnClick;
import ru.advantum.prototype.routebuilder.R;
import ru.advantum.prototype.routebuilder.annotation.FragmentDef;

@FragmentDef(resource = R.layout.fragment_create_route_actions, knifeEnabled = true)
public class CreateRouteFragment extends BaseAbstractFragment {

    private Callback mCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (Callback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement "
                    + getClass().getSimpleName() + " Callback interface");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null;
        super.onDetach();
    }

    @OnClick(R.id.bottom_action_close)
    public void onCancelClick() {
        if (mCallback != null) mCallback.onCancelCreateClick();
    }

    @OnClick(R.id.bottom_action_continue)
    public void onContinueClick() {
        if (mCallback != null) mCallback.onContinueCreateClick();
    }

    @OnClick(R.id.bottom_action_add)
    public void onAddCheckPointClick() {
        if (mCallback != null) mCallback.onAddCheckPointClick();
    }

    @OnClick(R.id.bottom_action_remove)
    public void onRemoveCheckPointClick() {
        if (mCallback != null) mCallback.onRemoveCheckPointClick();
    }


    public interface Callback {
        void onCancelCreateClick();

        void onContinueCreateClick();

        void onAddCheckPointClick();
        void onRemoveCheckPointClick();
    }
}
