package ru.advantum.prototype.routebuilder.logic;

import ru.advantum.prototype.routebuilder.model.CheckPoint;
import ru.advantum.prototype.routebuilder.model.Route;

/**
 * Created by Melcore on 21-May-16.
 */
public abstract class RouteMonitorLogic extends BaseLogic {
    public static Route getTestRoute() {
        Route route = new Route();
        route.setId(-1);
        route.setTitle("Test Route");
        route.setStartPoint(new CheckPoint(1, "Square", 5000L, 56.001269, 37.209910));
        route.setEndPoint(new CheckPoint(2, "Kryukovo", 15000L, 55.982782, 37.178183));
        route.addNextRelocation(new CheckPoint(3, "Panfill", 5000L, 56.007178, 37.202726));
        route.addNextRelocation(new CheckPoint(4, "12 region", 10000L, 55.994719, 37.188934));
        return route;
    }
}
