package ru.advantum.prototype.routebuilder;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;

import net.danlew.android.joda.JodaTimeAndroid;

import ru.advantum.prototype.routebuilder.util.PrefUtils;

/**
 * Base application
 */
public class RouteApp extends Application {

    private static RouteApp sInstance;

    public static RouteApp getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (sInstance == null) sInstance = this;
        JodaTimeAndroid.init(this);
        PrefUtils.init(this);

    }

    @SuppressWarnings("unused")
    public boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
