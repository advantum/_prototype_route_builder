package ru.advantum.prototype.routebuilder.logic;

import ru.advantum.prototype.routebuilder.R;
import ru.advantum.prototype.routebuilder.model.CheckPoint;

/**
 * Created by yurchenko on 06.05.2016.
 */
public class RelocationLogic extends BaseLogic {

    public static String getRelocationLabel(CheckPoint start, CheckPoint end) {
        return getContext().getString(R.string.template_relocation_title,
                CheckpointLogic.getCheckPointTitle(start),
                CheckpointLogic.getCheckPointTitle(end));
    }
}
