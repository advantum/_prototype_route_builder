package ru.advantum.prototype.routebuilder.ui;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ru.advantum.prototype.routebuilder.R;
import ru.advantum.prototype.routebuilder.logic.RouteMonitorLogic;
import ru.advantum.prototype.routebuilder.model.Route;
import ru.advantum.prototype.routebuilder.ui.widget.MovableContainerLayout;

public class MapsActivity extends BaseMapActivity implements OnMapReadyCallback, CreateRouteFragment.Callback {

    private static final String STATE_ACTION = "STATE_ACTION";
    @InjectView(R.id.toolbar)
    protected Toolbar mToolbar;
    @InjectView(R.id.right_panel_container)
    protected MovableContainerLayout mRightPanelFrame;
    @InjectView(R.id.bottom_panel_container)
    protected MovableContainerLayout mBottomPanelFrame;
    @InjectView(R.id.google_map)
    protected MapView mMapView;
    private GoogleMap mMap;
    private Action mAction = Action.VIEW;
    private MonitorRouteFragment mMonitorFagment;
    private Route mRoute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.inject(this);
        setSupportActionBar(mToolbar);
        mMapView.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mAction = (Action) savedInstanceState.getSerializable(STATE_ACTION);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.maps, menu);
        restoreActionBar();
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_item_action_new_road).setVisible(!Action.CREATE.equals(mAction));
        return super.onPrepareOptionsMenu(menu);
    }

    private void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) return;
        actionBar.setTitle("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_action_new_road:
                startRouteCreation();
                invalidateOptionsMenu();
                return true;
            case R.id.menu_item_action_start_monitor:
                showChooseRouteDialog();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showChooseRouteDialog() {
        // Now we have only one Route... lets do it ...
        mRoute = RouteMonitorLogic.getTestRoute();
        mAction = Action.MONITOR;

    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.getMapAsync(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(STATE_ACTION, mAction);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        mMap = null;
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        mMap = null;
        mMapView.onLowMemory();
        super.onLowMemory();
    }

    private void startRouteCreation() {
        addCreateRouteFragment();
        mAction = Action.CREATE;
    }

    private void addCreateRouteFragment() {
        if (getSupportFragmentManager().findFragmentByTag("CreateRouteFragment") == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(mBottomPanelFrame.getId(), new CreateRouteFragment(), "CreateRouteFragment")
                    .commit();
        }
        movePanel(mBottomPanelFrame, false, true);
    }

    private void addMonitorRouteFrgment(long id) {
        mMonitorFagment = (MonitorRouteFragment) getSupportFragmentManager()
                .findFragmentByTag("MonitorRouteFragment");
        if (mMonitorFagment == null) {
            mMonitorFagment = new MonitorRouteFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(mBottomPanelFrame.getId(), mMonitorFagment, "MonitorRouteFragment")
                    .commit();
        }
        mMonitorFagment.setRouteMonitor(id);
    }

    private void movePanel(MovableContainerLayout bottomPanelFrame, boolean hide, boolean animated) {
        bottomPanelFrame.changeState(hide, animated);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(0, 0);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    @Override
    public void onCancelCreateClick() {
        Toast.makeText(this, "Cancel clicked", Toast.LENGTH_SHORT).show();
        movePanel(mBottomPanelFrame, true, true);
        mAction = Action.VIEW;
        invalidateOptionsMenu();
    }

    @Override
    public void onContinueCreateClick() {
        Toast.makeText(this, "Continue clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddCheckPointClick() {
        Toast.makeText(this, "Add clicked", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onRemoveCheckPointClick() {
        Toast.makeText(this, "Remove clicked", Toast.LENGTH_SHORT).show();
    }

    public enum Action {
        VIEW, CREATE, MONITOR;

        private long routeId;

        public long getRouteId() {
            return routeId;
        }

        public void setRouteId(long routeId) {
            this.routeId = routeId;
        }
    }
}
