package ru.advantum.prototype.routebuilder.logic;

import android.text.TextUtils;

import ru.advantum.prototype.routebuilder.R;
import ru.advantum.prototype.routebuilder.model.CheckPoint;

/**
 * Created by yurchenko on 06.05.2016.
 */
public abstract class CheckpointLogic extends BaseLogic {

    public static String getCheckPointTitle(CheckPoint checkPoint) {
        return checkPoint == null || TextUtils.isEmpty(checkPoint.getTitle())
                ? getContext().getString(R.string.template_empty_checkpoint_label)
                : checkPoint.getTitle();
    }
}
