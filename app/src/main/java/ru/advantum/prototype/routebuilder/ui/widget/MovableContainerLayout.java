package ru.advantum.prototype.routebuilder.ui.widget;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;

import ru.advantum.prototype.routebuilder.R;

/**
 * Created by yurchenko on 06.05.2016.
 */
public class MovableContainerLayout extends FrameLayout {

    private ValueAnimator showAnim;
    private ValueAnimator hideAnim;
    private float mMovablePart = 1.0F;
    private boolean mIsMovingOutside = true;
    private boolean mIsHiddenOnStart = true;
    private Position mPosition = Position.BOTTOM;
    private long mDuration = 200l;
    private boolean mIsOldSdk = false;
    private int mMoveSize;
    private boolean mIsHidden = true;
    private boolean mIsMovingNow = false;

    public MovableContainerLayout(Context context) {
        super(context);
    }

    public MovableContainerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null) {
            TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.MovableContainerLayout);
            mMovablePart = array.getFloat(R.styleable.MovableContainerLayout_movablePart, mMovablePart);
            mIsMovingOutside = array.getBoolean(R.styleable.MovableContainerLayout_isMovingOutside, mIsMovingOutside);
            mIsHiddenOnStart = array.getBoolean(R.styleable.MovableContainerLayout_isMovedOnCreate, mIsHiddenOnStart);
            mIsHidden = mIsHiddenOnStart;
            mPosition = Position.values()[array.getInt(R.styleable.MovableContainerLayout_movablePart, mPosition.ordinal())];
            mDuration = array.getInteger(R.styleable.MovableContainerLayout_duration, (int) mDuration);
            array.recycle();
        }
        init();
    }

    public MovableContainerLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mMoveSize = mPosition == Position.BOTTOM || mPosition == Position.TOP ? h : w;
        initAnimations();
    }

    private void updateMoveSize() {
        mMoveSize = mPosition == Position.BOTTOM || mPosition == Position.TOP
                ? getHeight() : getWidth();
    }

    private void init() {
        mIsOldSdk = android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN;
        updateMoveSize();
        if (mMoveSize != 0 && mIsHidden) {
            setTranslationX(mMoveSize);
        }
        initAnimations();
    }

    private void initAnimations() {
        showAnim = ObjectAnimator.ofFloat(this, mPosition.value(), mMoveSize, 0f);
        showAnim.setDuration(mDuration);
        showAnim.setInterpolator(new DecelerateInterpolator());
        showAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                mIsMovingNow = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mIsMovingNow = false;
                mIsHidden = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        hideAnim = ObjectAnimator.ofFloat(this, mPosition.value(), 0f, mMoveSize);
        hideAnim.setDuration(mDuration);
        hideAnim.setInterpolator(new AccelerateInterpolator());
        hideAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                mIsMovingNow = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mIsMovingNow = false;
                mIsHidden = true;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public void changeState(boolean hide, boolean animated) {
        if (mIsMovingNow || mIsHidden == hide) return;
        if (hide) {
            if (!animated) hideAnim.setDuration(0);
            hideAnim.start();
            if (!animated) hideAnim.setDuration(mDuration);
        } else {
            if (!animated) showAnim.setDuration(0);
            showAnim.start();
            if (!animated) showAnim.setDuration(mDuration);
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return mIsMovingNow || super.onInterceptTouchEvent(ev);
    }

    public enum Position {
        START("translationX"), END("translationX"), TOP("translationY"), BOTTOM("translationY");

        private String value;

        Position(String value) {
            this.value = value;
        }

        public String value() {
            return value;
        }
    }
}
