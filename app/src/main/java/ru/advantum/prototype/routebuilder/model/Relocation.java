package ru.advantum.prototype.routebuilder.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import ru.advantum.prototype.routebuilder.logic.RelocationLogic;

/**
 * Created by yurchenko on 06.05.2016.
 */
@DatabaseTable
public class Relocation {

    @DatabaseField(generatedId = true, useGetSet = true)
    private long id;

    @DatabaseField(useGetSet = true, foreignAutoRefresh = true, foreignAutoCreate = true, foreign = true)
    private CheckPoint startPoint;

    @DatabaseField(useGetSet = true, foreignAutoRefresh = true, foreignAutoCreate = true, foreign = true)
    private CheckPoint endPoint;

    @DatabaseField(useGetSet = true)
    private String title;

    public Relocation() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CheckPoint getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(CheckPoint startPoint) {
        this.startPoint = startPoint;
        setTitle(RelocationLogic.getRelocationLabel(this.startPoint, this.endPoint));
    }

    public CheckPoint getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(CheckPoint endPoint) {
        this.endPoint = endPoint;
        setTitle(RelocationLogic.getRelocationLabel(this.startPoint, this.endPoint));
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
